const destinations = [
    {
        id: 1,
        type: 'zone',
        img: 'https://picsum.photos/300/200?image=244',
        name: 'Catalunya - Costa Brava',
        children: [
            {
                id: 2,
                type: 'zone',
                name: 'Roses - Cadaqués',
                children: [
                    {
                        id: 1,
                        type: 'establishment',
                        name: 'Hotel sol ixent',
                        establishment_type: 'bungalows',
                        category: '3',
                    },
                    {
                        id: 2,
                        type: 'establishment',
                        name: 'Hotel Vistabella',
                        establishment_type: 'bungalows',
                        category: '2',
                    },
                ],
            },
            {
                id: 3,
                type: 'zone',
                name: 'Roses - port de la selva',
                children: [
                    {
                        id: 3,
                        type: 'establishment',
                        name: 'Hotel semblant',
                        establishment_type: 'bungalows',
                        category: '1',
                    },
                    {
                        id: 4,
                        type: 'establishment',
                        establishment_type: 'bungalows',
                        name: 'Hotel 2',
                        category: '3',
                    },
                ],
            },
        ],
    },
    {
        id: 4,
        type: 'zone',
        img: 'https://picsum.photos/300/200?image=1024',
        name: 'Catalunya - Barceloneta',
        children: [
            {
                id: 5,
                type: 'zone',
                name: 'Calella - Costa de Barcelona - Maresme',
                children: [
                    {
                        id: 5,
                        type: 'establishment',
                        name: 'GHT Balmes Hotel, Aparthotel & SPLASH',
                        establishment_type: 'hotel',
                        category: '4',
                    },
                    {
                        id: 6,
                        type: 'establishment',
                        name: 'Hotel GHT Marítim',
                        establishment_type: 'bungalows',
                        category: '3',
                    },
                ],
            },
        ],
    },
    {
        id: 6,
        type: 'zone',
        img: '',
        name: 'LLeide',
        children: [
            {
                id: 19,
                type: 'zone',
                name: 'Tàrregue',
                children: [
                    {
                        id: 7,
                        type: 'establishment',
                        name: 'Hotel benasque',
                        establishment_type: 'hotel',
                        category: '1',
                    },
                ],
            },
        ],
    },
    {
        id: 7,
        type: 'zone',
        img: 'https://picsum.photos/300/200?image=616',
        name: 'Hotels de metro',
        children: [
            {
                id: 2,
                type: 'zone',
                name: 'Tàrregue',
                children: [
                    {
                        id: 8,
                        type: 'establishment',
                        name: 'Hotel sota terra',
                        establishment_type: 'hotel',
                        category: '2',
                    },
                ],
            },
        ],
    },
    {
        id: 8,
        type: 'zone',
        img: '',
        name: 'Hotels de metro',
        children: [
            {
                id: 2,
                type: 'zone',
                name: 'Tàrregue',
                children: [
                    {
                        id: 9,
                        type: 'establishment',
                        name: 'Hotel sota terra',
                        establishment_type: 'hotel',
                        category: '3',
                    },
                ],
            },
        ],
    },
    {
        id: 12,
        type: 'zone',
        name: 'Roses - Cadaqués',
        children: [
            {
                id: 11,
                type: 'establishment',
                name: 'Hotel sol ixent',
                establishment_type: 'bungalows',
                category: '3',
            },
            {
                id: 22,
                type: 'establishment',
                name: 'Hotel Vistabella',
                establishment_type: 'bungalows',
                category: '2',
            },
        ],
    },
    {
        id: 17,
        type: 'establishment',
        name: 'Hotel sol ixent',
        establishment_type: 'bungalows',
        category: '3',
    },
    {
        id: 28,
        type: 'establishment',
        name: 'Hotel Vistabella',
        establishment_type: 'bungalows',
        category: '2',
    },
]

module.exports = destinations
