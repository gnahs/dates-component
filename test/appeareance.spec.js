/* eslint-disable */
jest.setTimeout(2000)

const urlTest = 'http://dates-component.locale/test.html'
const urlVendor = 'http://dates-component.locale/public/js/vendor.js'
const urlComponent = 'http://dates-component.locale/public/js/dates-component.js'

const devices = require('puppeteer/DeviceDescriptors')
const iPhonex = devices['iPhone X']

let settings = {
    locale: {
        language:'es',
        format: 'DD-MM-YYYY'
    },
    appeareance:{
        mode: 'input',
        monthsShowed: 2,
        showNights: true,
    },
}

const addDays = (date, days) => {
    const copy = new Date(Number(date))
    copy.setDate(date.getDate() + days)
    return copy
}

const customBeforeAll = (settings) => {
    beforeAll (async () => {
        await page.goto(urlTest)
        await page.addScriptTag({ url: urlVendor })
        await page.addScriptTag({ url: urlComponent })
        await page.evaluate((settings) => {
            const DatesComponent = window.DatesComponent //otherwise the transpiler will rename it and won't work
            new DatesComponent('.dates-component',settings)
        }, settings)
    })
}

describe ('Appeareance modes', () => {

    customBeforeAll(settings)

    it('should create instance', async () => {
        await page.waitForSelector('.dates-component')
        let inputElement = await page.$('.dates-component')
        await expect(page.evaluate(element => element.getAttribute("class"), inputElement)).resolves.toMatch('dates-component--input')
    })

    it('should have two calendars', async () => {
        await page.waitForSelector('.dates-component_table')
        let tables = await page.$$('.dates-component_table')
        await expect(Promise.resolve(tables.length)).resolves.toBe(2)
    })

    it('should open calendar', async () => {
        await page.waitForSelector('.dates-component_check-in')
        let checkIn = await page.$('.dates-component_check-in')
        checkIn.click()
        await page.waitFor(600)
        let dropdownElement = await page.$('.dates-component_dropdown')
        await expect(page.evaluate(element => element.getAttribute("style"), dropdownElement)).resolves.toMatch('top:47px')
    })

    it('should pick first date', async () => {
        await page.waitForSelector('.dates-component_day:not(.dates-component_unselectable)')
        let dateElement = await page.$('.dates-component_day:not(.dates-component_unselectable)')
        dateElement.click()
        await page.waitFor(600)
        let date = new Date()
        await expect(page.evaluate(element => element.firstChild.textContent, dateElement)).resolves.toMatch(date.getDate().toString())
    })
})
