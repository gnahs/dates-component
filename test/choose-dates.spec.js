/* eslint-disable */
jest.setTimeout(2000)

const urlTest = 'http://dates-component.locale/test.html'
const urlVendor = 'http://dates-component.locale/public/js/vendor.js'
const urlComponent = 'http://dates-component.locale/public/js/dates-component.js'

const devices = require('puppeteer/DeviceDescriptors')
const iPhonex = devices['iPhone X']
const moment = require('moment')

let settings = {
    locale: {
        language:'es',
        format: 'MM-DD-YYYY',
        culture: null
    },
    appeareance:{
        mode: 'input',
        monthsShowed: 2,
        showNights: true,
    },
}

const customBeforeAll = (settings) => {
    beforeAll (async () => {
        await page.goto(urlTest)
        await page.addScriptTag({ url: urlVendor })
        await page.addScriptTag({ url: urlComponent })
        await page.evaluate((settings) => {
            const DatesComponent = window.DatesComponent //otherwise the transpiler will rename it and won't work
            new DatesComponent('.dates-component',settings)

            const userLanguage = window.navigator.language
            // Browser language (IE)
            || window.navigator.browserLanguage
            || 'en'

            const userLanguageArr = userLanguage.split('-')[0]
        }, settings)
    })
}

const addDays = (date, days) => {
    const copy = new Date(Number(date))
    copy.setDate(date.getDate() + days)
    return copy
}

describe ('Create and choose dates', () => {

    customBeforeAll(settings)

    it('should create instance', async () => {
        await page.waitForSelector('.dates-component')
        let inputElement = await page.$('.dates-component')
        await expect(page.evaluate(element => element.getAttribute("class"), inputElement)).resolves.toMatch('dates-component')
    })

    it('should open calendar', async () => {
        await page.waitForSelector('.dates-component_check-in')
        let checkIn = await page.$('.dates-component_check-in')
        checkIn.click()
        await page.waitFor(600)
        let dropdownElement = await page.$('.dates-component_dropdown')
        await expect(page.evaluate(element => element.getAttribute("style"), dropdownElement)).resolves.toMatch('top:47px')
    })

    it('should have two calendars', async () => {
        await page.waitForSelector('.dates-component_table')
        let tables = await page.$$('.dates-component_table')
        await expect(Promise.resolve(tables.length)).resolves.toBe(2)
    })

    it('should pick first date', async () => {
        await page.waitForSelector('.dates-component_day:not(.dates-component_unselectable)')
        let dateElement = await page.$('.dates-component_day:not(.dates-component_unselectable)')
        dateElement.click()
        await page.waitFor(600)
        let date = new Date()
        await expect(page.evaluate(element => element.firstChild.textContent, dateElement)).resolves.toMatch(date.getDate().toString())
    })

   it('should pick second date', async () => {
        await page.waitForSelector('.dates-component_day:not(.dates-component_unselectable')
        let dateElement = await page.$$('.dates-component_day:not(.dates-component_unselectable')
        dateElement[2].click()
        await page.waitFor(600)

        const date = new Date();
        const newDate = addDays(date, 2);

        await expect(page.evaluate(element => element.firstChild.textContent, dateElement[2])).resolves.toMatch(newDate.getDate().toString())
    })


    it('should be property input dated', async () => {
        await page.waitForSelector('.dates-component_check-in')
        let inputElementCheckIn = await page.$('.dates-component_check-in')
        let inputElementCheckOut = await page.$('.dates-component_check-out')

        const stringCheckIn = moment()
        const stringCheckOut = moment().add(2, 'days')
        moment.locale('en')

        await expect(page.evaluate(element => element.value, inputElementCheckIn)).resolves.toMatch(stringCheckIn.format(moment.localeData().longDateFormat('L')))
        await expect(page.evaluate(element => element.value, inputElementCheckOut)).resolves.toMatch(stringCheckOut.format(moment.localeData().longDateFormat('L')))
    })
})
