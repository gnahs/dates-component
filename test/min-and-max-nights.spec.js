/* eslint-disable */
jest.setTimeout(2000)
const moment = require('moment')
const urlTest = 'http://dates-component.locale/test.html'
const urlVendor = 'http://dates-component.locale/public/js/vendor.js'
const urlComponent = 'http://dates-component.locale/public/js/dates-component.js'

const devices = require('puppeteer/DeviceDescriptors')
const iPhonex = devices['iPhone X']
const inputSelector = '#datepicker-1'

const settings = {
    locale: {
        language:'es',
        format: 'MM-DD-YYYY',
        culture: null
    },
    appeareance:{
        mode: 'input',
        monthsShowed: 2,
        showNights: true,
    },
    dates:{
        nightsMin: 2,
        nightsMax: 3
    }
}

const customBeforeAll = (settings) => {
    beforeAll (async () => {
        await page.goto(urlTest)
        await page.addScriptTag({ url: urlVendor })
        await page.addScriptTag({ url: urlComponent })
        await page.evaluate((settings) => {
            const DatesComponent = window.DatesComponent //otherwise the transpiler will rename it and won't work
            new DatesComponent('.dates-component',settings)
        }, settings)
    })
}

const addDays = (date, days) => {
    const copy = new Date(Number(date))
    copy.setDate(date.getDate() + days)
    return copy
}

describe ('Create and choose dates', () => {

    customBeforeAll(settings)

    it('should create instance', async () => {
        await page.waitForSelector('.dates-component')
        let inputElement = await page.$('.dates-component')
        await expect(page.evaluate(element => element.getAttribute("class"), inputElement)).resolves.toMatch('dates-component--input')
    })

    it('should open calendar', async () => {
        await page.waitForSelector('.dates-component_check-in')
        let checkIn = await page.$('.dates-component_check-in')
        checkIn.click()
        await page.waitFor(600)
        let dropdownElement = await page.$('.dates-component_dropdown')
        await expect(page.evaluate(element => element.getAttribute("style"), dropdownElement)).resolves.toMatch('top:47px')
    })

    it('should pick first date', async () => {

        await page.waitForSelector('.dates-component_day:not(.dates-component_unselectable)')
        let dateElement = await page.$('.dates-component_day:not(.dates-component_unselectable)')
        dateElement.click()
        await page.waitFor(600)
    })


    it('should there available days', async () => {
        await page.waitForSelector('.dates-component_selected')
        let selected = await page.$$('.dates-component_selected')
        let nonUnSelected = await page.$$('.dates-component_day:not(.dates-component_unselectable)')

        await expect(Promise.resolve(selected.length)).resolves.toBe(3)
        await expect(Promise.resolve(nonUnSelected.length)).resolves.toBe(4)
    })

})
