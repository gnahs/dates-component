const webpack = require('webpack-boilerplate/webpack.config.js')
const MomentLocalesPlugin = require('moment-locales-webpack-plugin')

const momentPlugin = new MomentLocalesPlugin({
    localesToKeep: [ 'en', 'en-au', 'en-ca', 'en-gb', 'es', 'es-co', 'ca', 'fr', 'fr-ca', 'de', 'de-at', 'ru', 'nl', 'it', 'pt', 'pt-br', 'es-ec', 'es-ar', 'pt-br' ],
})

webpack.entry = {
    'dates-component': 'js/src/js/entry-points/app.js',
}

webpack.plugins.push(momentPlugin)
module.exports = webpack
