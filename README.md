# rho-occupancy-selector

## Usage

1. Import component:

```js
    import OccupancyComponent from 'occupancy-component/src/js/dates-component'
```


2. Import POSTCSS:

```css
    @import '../../node_modules/dates-component/src/css/settings';
```

You can overrite some variables:
```sass
    // GENERAL
    $dates-component-font-family: 'Lato', sans-serif;
    $dates-component-base-color: #345379;
    $dates-component-text-color: #212529;
    $dates-component-line-height: 1.428571429em;
    $dates-component-border-color: #d7d7d7;
    $dates-component-border-radius: 3px;

    // INPUT
    $dates-component-input-height: 40px;
    $dates-component-input-font-size: .9em;
    $dates-component-input-box-shadow: 0px 1px 2px 0px rgba(176,176,176,0.5);

    // DROPPDOWN
    $occupancy-component-dropdown-width: 410px;
    $occupancy-component-dropdown-box-shadow: 0px 4px 9px 1px rgba(176,176,176,1);

    // BUTTON
    $occupancy-component-button-font-color: #555;
    $occupancy-component-button-padding: .375em .75em;
```

3. Initialize:

```javascript
    var selector = document.querySelector('dates-selector')
    if (selector === null) return
    this.$occupancy = new OccupancyComponent('dates-selector')
```


## Default Options

Can be override on plugin init:


```javascript
var defaults = {
   // LOCALES
    allowedCultures: [ 'en', 'en-au', 'en-ca', 'en-gb', 'es', 'es-co', 'ca', 'fr', 'fr-ca', 'de', 'de-at', 'ru', 'nl', 'it', 'pt', 'pt-br', 'es-ec', 'es-ar', 'pt-br' ],
    locale: {
        format: 'YYYY-MM-DD',
        language: 'es',
        culture: 'auto',
        literals: Literals,
    },
    // DATE SETTINGS
    dates: {
        checkIn: null,
        checkOut: null,
        checkInMin: null,
        checkInMax: null,
        nightsMin: 1,
        nightsMax: 0,
        disabled: [], // {from:2019-09-02, to: 2019-09-12}
        enabledCheckIn: [], // according to the locale format
        enabledCheckOut: [], // according to the locale format
        disabledWeekDays: [],
        busy: [], // {from:2019-09-02, to: 2019-09-12} FEATURE
        single: false,
        end: null,
    },
    // APPEREANCE
    appeareance: {
        // input / custom / default
        mode: 'input',
        orientation: 'auto', // Auto, top, bottom
        monthsShowed: 2,
        showNights: true,
        expanded: false,
        autoClose: true,
        checkInSelector: null,
        checkOutSelector: null,
        offset: {
            checkInTop: 0,
            checkInLeft: 0,
            checkOutTop: 0,
            checkOutLeft: 0,
        },
    },
    // CUSTOM FUNCTIONS
    customFunctions: {
        onDayClick: false,
        onOpenDatepicker: false,
        onCloseDatepicker: false,
    },
};
```


## Events

You can bind the following events:

### checkIn

Fired when the user change checkinDate.

```javascript
     var selector = document.querySelector('dates-component)
        if (selector === null) return
        selector.addEventListener('checkIn', (ev) => {
            selector.querySelector('.check-in-selector .day-number').innerHTML = ev.detail.dayOfMonth
            selector.querySelector('.check-in-selector .day').innerHTML = ev.detail.dayOfWeek
            selector.querySelector('.check-in-selector .month').innerHTML = `${ev.detail.month}, ${ev.detail.year}`

            this.userSelection.dates.checkIn = ev.detail.format

        }, false);
```

### checkOut

Fired when the user change checkinDate.

```javascript
    var selector = document.querySelector('dates-component')
    if (selector === null) return
    selector.addEventListener('checkOut', (ev) => {
        selector.querySelector('.check-out-selector .day-number').innerHTML = ev.detail.dayOfMonth
        selector.querySelector('.check-out-selector .day').innerHTML = ev.detail.dayOfWeek
        selector.querySelector('.check-out-selector .month').innerHTML = `${ev.detail.month}, ${ev.detail.year}`

        this.userSelection.dates.checkOut = ev.detail.format

    }, false);

```