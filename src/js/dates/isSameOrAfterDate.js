import { isAfterDate } from './isAfterDate'
import { isSameDate } from './isSameDate'

/**
 *
 * @param {Date} date
 * @param {Date} date1
 * @returns {Boolean}
 */
function isSameOrAfterDate(date, date1) {
    return isSameDate(date, date1) || isAfterDate(date, date1)
}

export { isSameOrAfterDate }
