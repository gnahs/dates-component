import { sumDate } from './sumDate'

/**
 * @param {Date} date Date
 * @returns
 */
function nextMonth(date) {
    return sumDate(date, 1, 'month')
}

export {
    nextMonth,
}
