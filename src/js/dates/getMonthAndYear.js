/**
 *
 * @param {Date} date
 * @param {string} locale
 * @returns string
 */
function getMonthAndYear(date, locale) {
    const monthDate = date !== null ? date : new Date()
    const monthAndYear = Intl.DateTimeFormat(locale, { month: 'long', year: 'numeric' }).format(monthDate)

    return monthAndYear
}

export {
    getMonthAndYear,
}
