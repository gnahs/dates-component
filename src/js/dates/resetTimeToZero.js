/**
 *
 * @param {Date} date
 * @returns Date
 */
function resetTimeToZero(date) {
    date.setHours(0, 0, 0, 0)
    return date
}
export { resetTimeToZero }
