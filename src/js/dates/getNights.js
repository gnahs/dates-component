import { turnTo } from './turnTo'

/**
 * @param {Date} date
 * @param {Date} date1
 * @returns number
 */
function getNights(date, date1) {
    const differenceInTime = date1.getTime() - date.getTime()
    const differenceInDays = turnTo(differenceInTime, 'day')

    return Math.ceil(differenceInDays)
}

export {
    getNights,
}
