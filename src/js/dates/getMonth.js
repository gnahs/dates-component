/* eslint-disable import/prefer-default-export */

/**
 * @param date {Date}
 * @param locale {string}
 * @param style {('numeric' | '2-digit' | 'long' | 'short' | 'narrow' | undefined)}
 * @returns {string}
 */
function getMonth(date, locale, style) {
    return Intl.DateTimeFormat(locale, {
        month: style,
    }).format(date)
}

export {
    getMonth,
}
