import { Info } from 'luxon'

/**
 * @param {'narrow' | 'short' | 'long'} length
 * @param {string} locale
 * @returns string[]
 */
function getWeekDays(length, locale = 'en-US') {
    return Info.weekdays(length, {
        locale,
    })
}

export {
    getWeekDays,
}
