/**
 * @param {Date} date
 * @param {number} days
 * @returns Date
 */
function substractDays(date, days) {
    const internalDate = new Date(date.getTime())
    internalDate.setDate(internalDate.getDate() - days)

    return internalDate
}

export {
    substractDays,
}
