/**
 *
 * @param {number} month
 * @param {number} year
 * @returns number
 */
function getNumOfDaysInMonth(month, year) {
    return new Date(year, month + 1, 0).getDate()
}

export {
    getNumOfDaysInMonth,
}
