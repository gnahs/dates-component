const SUM_UNIT_DATE = {
    day: (date, number) => new Date(date.getFullYear(), date.getMonth(), date.getDate() - number),
    month: (date, number) => new Date(date.getFullYear(), date.getMonth() - number, date.getDate()),
    year: (date, years) => new Date(date.getFullYear() - years, date.getMonth(), date.getDate()),
}

/**
 * @param {Date} date
 * @param {number} number
 * @param {'year'|'month'|'day'} unit
 * @returns Date
 */
function substractDate(date, number, unit = 'day') {
    return SUM_UNIT_DATE[unit](date, number)
}

export { substractDate }
