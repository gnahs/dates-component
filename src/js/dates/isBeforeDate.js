import { getUnitDate } from './getUnitDate'
import { resetTimeToZero } from './resetTimeToZero'

/**
 *
 * @param {Date} date
 * @param {Date} date1 - date to compare
 * @param {'day'|'month'|'year'|undefined} unit
 * @returns Boolean
 */
function isBeforeDate(date, date1, unit = undefined) {
    const [ firstDate, secondDate ] = unit
        ? getUnitDate(unit, date)
        : [ resetTimeToZero(date).getTime(), resetTimeToZero(date1).getTime() ]

    return firstDate < secondDate
}

export {
    isBeforeDate,
}
