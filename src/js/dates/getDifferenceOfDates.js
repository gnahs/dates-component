import { resetTimeToZero } from './resetTimeToZero'
import { turnTo } from './turnTo'


/**
 * @param {Date} date
 * @param {Date} date1
 * @param {'day'|'month'|'year'|'millisecond'} unit
 * @return number
 */
function getDifferenceOfDates(date, date1, unit = 'millisecond') {
    const timeDifference = resetTimeToZero(date).getTime() - resetTimeToZero(date1).getTime()
    const differeneInUnit = turnTo(timeDifference, unit)

    return differeneInUnit
}


export {
    getDifferenceOfDates,
}
