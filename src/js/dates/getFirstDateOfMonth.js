/* eslint-disable import/prefer-default-export */

/**
 * @param date {Date}
 * @returns {Date}
 */
function getFirstDateOfMonth(date) {
    return new Date(date.getFullYear(), date.getMonth(), 1)
}

export {
    getFirstDateOfMonth,
}
