import { isAfterDate } from './isAfterDate'
import { isBeforeDate } from './isBeforeDate'
import { isSameDate } from './isSameDate'

/**
 *
 * @param {Date} date - date to compare
 * @param {Date} date1 - min date (inclusive)
 * @param {Date} date2 - max date (inclusive)
 * @returns boolean
 */
function isBetweenTwoDates(date, date1, date2) {
    const isAfterOrSameDate = isAfterDate(date, date1) || isSameDate(date, date1)
    const isBeforeOrSameDate = isBeforeDate(date, date2) || isSameDate(date, date2)

    return isAfterOrSameDate && isBeforeOrSameDate
}

export {
    isBetweenTwoDates,
}
