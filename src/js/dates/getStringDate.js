/* eslint-disable import/prefer-default-export */

/**
 * @param {Date} date
 * @param {string} locale
 * @param {Object} options
 * @returns string
 */
function getStringDate(date, locale = 'en-US', options = { month: '2-digit', day: 'numeric', year: 'numeric' }) {
    return new Intl.DateTimeFormat(locale, options).format(date)
}

export {
    getStringDate,
}
