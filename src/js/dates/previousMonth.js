import { substractDate } from './substractDate'

/**
 * @param {Date} date
 * @returns Date
 */
function previousMonth(date) {
    return substractDate(date, 1, 'month')
}

export {
    previousMonth,
}
