import { getWeekDayNumber } from './getWeekDayNumber'
import { substractDays } from './substractDays'
import { getFirstDateOfMonth } from './getFirstDateOfMonth'

/**
 *
 * @param {Date} date
 * @returns Date
 */
function getDateOfFirstMondayOfMonth(date) {
    const firstDate = getFirstDateOfMonth(date)
    const weekDayNumber = getWeekDayNumber(firstDate)

    return substractDays(firstDate, weekDayNumber)
}

export {
    getDateOfFirstMondayOfMonth,
}
