import { getNumOfDaysInMonth } from './getNumOfDaysInMonth'

/**
 * @param {Date} date
 * @returns Date[]
 */
function getDaysInMonth(date) {
    const month = date.getMonth()
    const year = date.getFullYear()
    const numOfDaysInMonth = getNumOfDaysInMonth(month, year)

    const daysInMonth = Array.from({ length: numOfDaysInMonth }, (_, i) => {
        const day = i + 1
        const dateOfDay = new Date(year, month, day)

        return dateOfDay
    })

    return daysInMonth
}

export {
    getDaysInMonth,
}
