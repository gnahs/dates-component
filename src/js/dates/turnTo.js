const UNIT_IN_MILISECONDS = {
    millisecond: 1,
    day: 1000 * 3600 * 24,
    month: 1000 * 3600 * 24 * 30,
    year: 1000 * 3600 * 24 * 365,
}

/**
 * @param {number} miliseconds
 * @param {'day'|'month'|'year'|'millisecond'} unit
 * @returns {number}
 */
function turnTo(miliseconds, unit) {
    return Math.floor(miliseconds / UNIT_IN_MILISECONDS[unit])
}

export {
    turnTo,
}
