import { getUnitDate } from './getUnitDate'
import { resetTimeToZero } from './resetTimeToZero'
/**
 *
 * @param {Date} date
 * @param {Date} date1 - date to compare
 * @param {'day'|'month'|'year'|null} unit
 * @returns Boolean
 */
function isAfterDate(date, date1, unit = null) {
    const [ firstDate, secondDate ] = unit
        ? getUnitDate(unit, date)
        : [ resetTimeToZero(date).getTime(), resetTimeToZero(date1).getTime() ]

    return firstDate > secondDate
}

export {
    isAfterDate,
}
