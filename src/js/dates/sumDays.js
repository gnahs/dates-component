/* eslint-disable import/prefer-default-export */

/**
 * @param {Date} date
 * @param {number} days
 * @returns Date
 */
function sumDays(date, days) {
    const internalDate = new Date(date.getTime())
    internalDate.setDate(internalDate.getDate() + days)

    return internalDate
}

export {
    sumDays,
}
