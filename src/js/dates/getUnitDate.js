const UNIT_DATE = {
    day: date => date.getDate(),
    month: date => date.getMonth(),
    year: date => date.getFullYear(),
}

/**
 *
 * @param {'day'|'month'|'year'} unit
 * @param  {...Date} dates
 * @returns number[]
 */
function getUnitDate(unit, ...dates) {
    return dates.map(date => UNIT_DATE[unit](date))
}


export {
    getUnitDate,
}
