import { getUnitDate } from './getUnitDate'
import { isBeforeDate } from './isBeforeDate'
import { isSameDate } from './isSameDate'

/**
 *
 * @param {Date} date
 * @param {Date} date1
 * @param {'day'|'month'|'year'|null} unit
 * @returns boolean
 */
function isSameOrBeforeDate(date, date1, unit = null) {
    return isSameDate(date, date1) || isBeforeDate(date, date1)
}

export { isSameOrBeforeDate }
