/**
 *
 * @param {Date} date1
 * @param {Date} date2
 * @returns Boolean
 */
function isSameDate(date1, date2) {
    if (date1 === null || date2 === null) return false
    return date1.getFullYear() === date2.getFullYear()
    && date1.getMonth() === date2.getMonth()
    && date1.getDate() === date2.getDate()
}

export {
    isSameDate,
}
