import { getWeekDayNumber } from './getWeekDayNumber'
import { getWeekDays } from './getWeekDays'

/**
 *
 * @param {Date} date
 * @param {string} locale
 * @param {'narrow' | 'short' | 'long'} length
 * @returns string
 */
function getWeekDay(date, locale, length = 'short') {
    const weekDayNumber = getWeekDayNumber(date)
    return getWeekDays(length, locale)[weekDayNumber]
}

export {
    getWeekDay,
}
