import { sumDays } from './sumDays'
import { getDateOfFirstMondayOfMonth } from './getDateOfFirstMondayOfMonth'
import { nextMonth } from './nextMonth'
import { getWeekDayNumber } from './getWeekDayNumber'

/**
 * @param {Date} date
 * @returns Date[]
 */
function getDatesStartingFirstMondayOfMonth(date) {
    const dateToStart = getDateOfFirstMondayOfMonth(date)

    const dates = new Array(42)
        .fill(dateToStart)
        .map((currentDate, index) => {
            const nextDay = sumDays(currentDate, index)

            return nextDay
        })

    return dates
}

export {
    getDatesStartingFirstMondayOfMonth,
}
