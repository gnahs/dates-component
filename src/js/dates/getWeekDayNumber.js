/**
 * @param {Date} date
 * @returns {number} 0 to 6 (monday to sunday)
 */
function getWeekDayNumber(date) {
    if (date === null) return 6
    const firstDayOfMonth = date.getDay()

    return firstDayOfMonth === 0 ? 6 : firstDayOfMonth - 1
}

export {
    getWeekDayNumber,
}
