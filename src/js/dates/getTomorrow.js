
/**
 *
 * @param {Date} date
 * @returns Date
 */
function getTomorrow(date = new Date()) {
    const tomorrow = date
    tomorrow.setDate(tomorrow.getDate() + 1)

    return tomorrow
}

export {
    getTomorrow,
}
