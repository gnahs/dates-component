export default {
    from: 'de',
    to: 'a',
    checkin_date: 'Data de check-in',
    checkout_date: 'Data de checkout',
    night: 'noite |||| noites',
    Today: 'Hoje',
    Tomorrow: 'Amanhã',
}
