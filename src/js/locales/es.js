export default {
    from: 'del',
    to: 'al',
    checkin_date: 'Fecha de entrada',
    checkout_date: 'Fecha de salida',
    night: 'noche |||| noches',
    Today: 'Hoy',
    Tomorrow: 'Mañana',
}
