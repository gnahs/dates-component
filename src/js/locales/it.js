export default {
    from: 'da',
    to: 'a',
    checkin_date: 'Data check-in',
    checkout_date: 'Data check-out',
    night: 'notte |||| notti',
    Today: 'Oggi',
    Tomorrow: 'Domani',
}
