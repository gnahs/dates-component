export default {
    from: 'от',
    to: 'до',
    checkin_date: 'дата приезда',
    checkout_date: 'дата отъезда',
    night: 'ночь |||| ночей |||| ночей',
    Today: 'Сегодня',
    Tomorrow: 'Завтра',
}
