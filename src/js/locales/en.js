export default {
    from: 'from',
    to: 'to',
    checkin_date: 'Check-in date',
    checkout_date: 'Check-out date',
    night: 'night |||| nights',
    Today: 'Today',
    Tomorrow: 'Tomorrow',
}
