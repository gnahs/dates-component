export default {
    from: 'vanaf',
    to: 'tot',
    checkin_date: 'Incheckdatum',
    checkout_date: 'Uitcheckdatum',
    night: 'nacht |||| nachten',
    Today: 'Vandaag',
    Tomorrow: 'Morgen',
}
