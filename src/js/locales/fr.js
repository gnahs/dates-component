export default {
    from: 'de',
    to: 'à',
    checkin_date: 'Date d\'arrivée',
    checkout_date: 'Date de départ',
    night: 'nuit |||| nuits',
    Today: 'Aujourd\'hui',
    Tomorrow: 'Demain',
}
