import ca from './ca'
import de from './de'
import en from './en'
import es from './es'
import fr from './fr'
import it from './it'
import nl from './nl'
import pt from './pt'
import ru from './ru'

export default {
    ca, de, en, es, fr, it, nl, pt, ru,
}
