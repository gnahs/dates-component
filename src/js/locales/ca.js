export default {
    from: 'del',
    to: 'al',
    checkin_date: 'Data d\'entrada',
    checkout_date: 'Data de sortida',
    night: 'nit |||| nits',
    Today: 'Avui',
    Tomorrow: 'Demà',
}
