export default {
    from: 'von',
    to: 'bis',
    checkin_date: 'Anreisedatum',
    checkout_date: 'Abreisedatum',
    night: 'Nacht |||| Nächte',
    Today: 'heute',
    Tomorrow: 'morgen',
}
