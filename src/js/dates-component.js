/* eslint-disable class-methods-use-this */

/* eslint-disable max-len */
import Merge from 'deepmerge'
import { elementPosition, h, isMobile } from 'utils/utils'
import {
    arrowLeft,
    arrowRight,
    close,
    moon,
} from './svg'
import defaultSettings from './default-settings'
import Translate from './Translate'

// Date functions
import { getFirstDateOfMonth } from './dates/getFirstDateOfMonth'
import { sumDays } from './dates/sumDays'
import { substractDays } from './dates/substractDays'
import { getStringDate } from './dates/getStringDate'
import { getWeekDay } from './dates/getWeekDay'
import { getWeekDays } from './dates/getWeekDays'
import { getDatesStartingFirstMondayOfMonth } from './dates/getDatesStartingFirstMondayOfMonth'
import { isBeforeDate } from './dates/isBeforeDate'
import { isAfterDate } from './dates/isAfterDate'
import { isSameDate } from './dates/isSameDate'
import { isBetweenTwoDates } from './dates/isBetweenTwoDates'
import { getDifferenceOfDates } from './dates/getDifferenceOfDates'
import { isSameOrAfterDate } from './dates/isSameOrAfterDate'
import { resetTimeToZero } from './dates/resetTimeToZero'
import { sumDate } from './dates/sumDate'
import { getMonthAndYear } from './dates/getMonthAndYear'
import { nextMonth } from './dates/nextMonth'
import { previousMonth } from './dates/previousMonth'
import Utils from './utils'

/** @typedef {import('./types/settings.js').Settings} */
/** @typedef {import('./types/moments').Moments} */

/**
 * @typef {Object} MonthDay
 * @property {Date} date
 * @property {string} class
 * */

class DatesComponent {
    constructor(selector, options) {
        /** @type Settings  */
        this.settings = Merge(defaultSettings, options)
        this.$ = {
            mainElement: document.querySelector(selector),
        }
        // eslint-disable-next-line spaced-comment
        this.eventType = 'click'//(isMobile()) ? 'touchend' : 'click'
        this.instance = this
        // this.eventType = (isMobile()) ? 'touchend' : 'click'

        this.trans = null

        /** @type Moments */
        this.moments = {}
        this.events = {
            onClickNextMonth: this.onClickNextMonth.bind(this),
            onClickPrevMonth: this.onClickPrevMonth.bind(this),
            onClickDay: this.onClickDay.bind(this),
            onHoverDay: this.onHoverDay.bind(this),
            onLeaveDay: this.onLeaveDay.bind(this),
            onClickCheckInSelector: this.onClickCheckInSelector.bind(this),
            onClickCheckOutSelector: this.onClickCheckOutSelector.bind(this),
            onClickOutside: this.onClickOutside.bind(this),
            onClickClose: this.onClickClose.bind(this),
        }

        this.isCLickRoleCheckIn = true
        this.triggerCheckOut = true
        this.initCulture()
            .initLanguage()
            .initAppearance()
            .initDates()
            .initMoments()
            .initCalendar()
            .initPermanentEvents()
            .initMobile()

        return this
    }

    initLanguage() {
        this.trans = Translate.instance
        return this
    }

    initCalendar() {
        const dropdownSelector = document.createElement('div')
        dropdownSelector.setAttribute('class', 'dates-component_dropdown dates-component')
        dropdownSelector.setAttribute('style', 'display:none')
        this.$.mainElement.append(dropdownSelector)
        this.$.dropdown = dropdownSelector
        this.reloadCalendar()
        return this
    }

    /** @param {(Date|null)} month */
    reloadCalendar(month = null) {
        this.$.dropdown.innerHTML = this.createMonthsDom(month)
        this.appendHeader()
        if (isMobile()) document.body.append(this.$.dropdown)
        this.initEvents()
    }

    appendHeader() {
        if (!isMobile()) return
        const headerString = this.isCLickRoleCheckIn
            ? this.trans.translate('checkin_date')
            : this.trans.translate('checkout_date')
        // const header = h('div', { class: 'dates-component_header' }, `${headerString} ${close}`)

        const header = h('div', { class: 'dates-component_header' }) // , `${headerString} ${close}`)
        header.innerHTML = `${headerString} ${close}`

        this.$.dropdown.prepend(header)
    }

    initAppearance() {
        if (this.settings.appeareance.mode === 'custom') {
            this.$.checkIn = this.$.mainElement.querySelector(this.settings.appeareance.checkInSelector)
            this.$.checkOut = this.$.mainElement.querySelector(this.settings.appeareance.checkOutSelector)
            this.$.mainElement.classList.add('custom')
            return this
        }

        this.initInputMode()
        return this
    }

    initMobile() {
        if (!isMobile()) return

        this.$.dropdown.classList.add('dates-component_dropdown--mobile')
        this.settings.appeareance.monthsShowed = 1
    }

    initInputMode() {
        if (this.settings.appeareance.expanded) this.$.mainElement.classList.add('dates-component--expanded')
        this.$.mainElement.classList.add('dates-component--input')
        const inputCheckIn = h('input', { 'check-in': true, class: 'dates-component_input dates-component_check-in', readOnly: 'readonly' })
        const inputCheckOut = h('input', { 'check-out': true, class: 'dates-component_input dates-component_check-out', readOnly: 'readonly' })
        const inputContainer = h('div', { class: 'dates-component-container-inputs' }, inputCheckIn, inputCheckOut)
        this.$.checkIn = inputCheckIn
        this.$.checkOut = inputCheckOut
        this.$.mainElement.prepend(inputContainer)
    }

    initCulture() {
        // User language is valid language
        if (this.settings.locale.culture !== 'auto'
        && this.settings.allowedCultures.includes(this.settings.locale.culture)) {
            return this
        }

        // Browser language
        const userLanguage = window.navigator.language
        // Browser language (IE)
        || window.navigator.browserLanguage
        || 'en'

        const userLanguageArr = userLanguage.split('-')[0]

        if (this.settings.allowedCultures.includes(userLanguageArr)) {
            this.settings.locale.culture = userLanguageArr
            return this
        }

        this.settings.locale.culture = 'en'
        return this
    }

    // -----------------------
    // INIT DATES FUNCTIONS
    // -----------------------
    initDates() {
        this.initCalendarLimits()
        this.settings.dates.checkIn = this.settings.dates.checkIn || resetTimeToZero(new Date())
        this.settings.dates.checkOut = this.settings.dates.checkOut || sumDays(new Date(), this.settings.dates.nightsMin)
        this.settings.dates.checkInMin = this.settings.dates.checkInMin || resetTimeToZero(new Date())
        this.settings.dates.checkInMax = this.settings.dates.checkInMax || substractDays(this.settings.dates.end, this.settings.dates.nightsMax)
        return this
    }

    initCalendarLimits() {
        this.settings.dates.start = this.settings.dates.start
            ? getFirstDateOfMonth(new Date(this.settings.dates.start))
            : getFirstDateOfMonth(new Date())

        this.settings.dates.end = this.settings.dates.end
            ? getFirstDateOfMonth(new Date(this.settings.dates.end))
            : sumDate(new Date(), 2, 'year')
    }

    initMoments() {
        const names = [ 'start', 'end', 'checkIn', 'checkOut', 'start', 'checkInMin', 'checkInMax', 'lastCheckIn' ]
        names.forEach((name) => {
            this.moments[name] = resetTimeToZero(new Date(this.settings.dates[name]))
        })
        this.initDisabledMoments()
        this.moments.current = new Date(this.settings.dates.start)
        if (this.settings.dates.checkIn !== null) {
            this.setCheckInSelector()
            this.setCheckInEvent()
        }
        if (this.settings.dates.checkOut !== null) {
            this.setCheckOutSelector()
            this.setCheckOutEvent()
        }
        return this
    }

    initDisabledMoments() {
        this.moments.disabled = this.settings.dates.disabled.map(range => ({
            from: new Date(range.from),
            to: new Date(range.to),
        }))
    }


    // ----------------
    // EVENTS
    // ----------------
    initEvents() {
        const nextSelector = this.$.dropdown.querySelector('.dates-component_next-month')
        const prevSelector = this.$.dropdown.querySelector('.dates-component_prev-month')
        const closeSelector = this.$.dropdown.querySelector('.dates-component_close')

        if (isMobile() && closeSelector !== null) closeSelector.addEventListener(this.eventType, this.events.onClickClose, true)
        if (nextSelector !== null) {
            nextSelector.addEventListener(this.eventType, this.events.onClickNextMonth, true)
        }
        if (prevSelector !== null) {
            prevSelector.addEventListener(this.eventType, this.events.onClickPrevMonth, true)
        }

        return this
    }

    initPermanentEvents() {
        // const daySelector = this.$.mainElement.querySelector('.dates-component_dropdown')
        const daySelector = this.$.dropdown

        daySelector.addEventListener(this.eventType, this.events.onClickDay, true)

        if (this.settings.appeareance.showNights) daySelector.addEventListener('mouseover', this.events.onHoverDay, true)
        if (this.settings.appeareance.showNights) daySelector.addEventListener('mouseout', this.events.onLeaveDay, true)

        this.$.checkIn.addEventListener('touchstart', this.events.onClickCheckInSelector, true)
        this.$.checkIn.addEventListener('touchmove', this.events.onClickCheckInSelector, true)

        this.$.checkIn.addEventListener(this.eventType, this.events.onClickCheckInSelector, true)
        this.$.checkOut.addEventListener(this.eventType, this.events.onClickCheckOutSelector, true)

        return this
    }

    deleteEvents() {
        const nextSelector = this.$.dropdown.querySelector('.dates-component_next-month')
        const prevSelector = this.$.dropdown.querySelector('.dates-component_prev-month')
        if (nextSelector !== null) {
            nextSelector.removeEventListener(this.eventType, this.events.onClickNextMonth, true)
        }
        if (prevSelector) {
            prevSelector.removeEventListener(this.eventType, this.events.onClickPrevMonth, true)
        }
    }

    preventOpenCalendar(ev) {
        if (ev.type === 'touchstart') {
            this.moved = false
            return true
        }

        if (ev.type === 'touchmove') {
            this.moved = true
            return true
        }

        if (this.moved) return true
        return false
    }

    onClickCheckInSelector(ev) {
        if (this.preventOpenCalendar(ev)) return
        this.isCLickRoleCheckIn = true
        this.reloadCalendar(this.moments.current)
        this.open()
        this.setDropdownPosition()
        this.setOpenCheckInEvent()
    }

    onClickCheckOutSelector(ev) {
        if (this.preventOpenCalendar(ev)) return
        this.isCLickRoleCheckIn = false
        // if checkin date is in last month
        this.moments.current = getFirstDateOfMonth(this.moments.checkIn)
        this.reloadCalendar(this.moments.current)
        this.open()
        this.setDropdownPosition()
        this.setOpenCheckOutEvent()
    }

    onClickOutside(ev) {
        if (!this.$.dropdown.contains(ev.target)) {
            this.isCLickRoleCheckIn = true
            if (this.moments.checkOut === null) {
                this.addDaysToCheckOutMomentIfIsAfter()
            }
            this.close()
        }
    }

    onClickDay(ev) {
        ev.preventDefault()
        if (this.settings.customFunctions.onDayClick) this.settings.customFunctions.onDayClick(ev)
        if (!ev.target.classList.contains('dates-component_day') || ev.target.classList.contains('dates-component_unselectable')) return

        const date = ev.target.getAttribute('data-date')

        if (this.isCLickRoleCheckIn) {
            this.onClickCheckIn(date)
            this.setDropdownPosition()
            return
        }

        this.onClickCheckOut(date)
    }

    /**
     * @param {Date} date
     * @return HTMLELEMENT
     */
    createMoon(date) {
        const nights = this.getNights(date)
        const tooltipClass = this.settings.appeareance.showMoon
            ? 'dates-component_tooltip dates-component_tooltip--moon'
            : 'dates-component_tooltip'
        const moonContent = this.settings.appeareance.showMoon
            ? moon
            : ''

        const moonHTML = h('div', { class: 'moon' }, moonContent)
        const textHTML = h('div', {}, moonHTML, `${nights} ${this.trans.translate('night', 2)}`)
        const tooltipHTML = h('div', { class: tooltipClass }, textHTML)

        return tooltipHTML
    }

    onHoverDay(ev) {
        if (
            this.isCLickRoleCheckIn
            || !ev.target.classList.contains('dates-component_day')
            || ev.target.classList.contains('dates-component_unselectable')
        ) {
            return
        }

        const dateString = ev.target.getAttribute('data-date')
        const date = new Date(dateString)
        if (isSameDate(date, this.moments.checkIn)) return

        if (this.settings.appeareance.showNights) {
            const nightDiv = this.createMoon(date)
            ev.target.append(nightDiv)

            if (this.settings.appeareance.showMoon) nightDiv.querySelector('.moon').innerHTML = moon

            const { width, height } = elementPosition(nightDiv)
            nightDiv.style.left = `-${(width - 38) / 2}px`
            nightDiv.style.top = `-${height}px`
        }
    }

    onLeaveDay(ev) {
        if (this.isCLickRoleCheckIn) return

        const selector = ev.target.querySelector('.dates-component_tooltip')
        if (this.settings.appeareance.showNights && selector !== null) {
            selector.remove()
        }
    }

    onClickClose() {
        if (this.moments.checkOut === null) {
            this.addDaysToCheckOutMomentIfIsAfter()
        }
        this.close()
    }

    /** @param {string} date */
    onClickCheckIn(date) {
        this.moments.lastCheckIn = this.moments.checkIn || resetTimeToZero(new Date(date))
        this.moments.checkIn = resetTimeToZero(new Date(date))
        // this.addDaysToCheckOutMomentIfIsAfter()
        this.isCLickRoleCheckIn = false
        this.moments.current = getFirstDateOfMonth(this.moments.checkIn)
        this.reloadCalendar(this.moments.current)
        this.setCheckInSelector()
        this.setCheckInEvent()
        this.triggerCheckOut = true
        this.moments.checkOut = null
    }

    /** @param {string} date */
    onClickCheckOut(date) {
        const newCheckOut = resetTimeToZero(new Date(date))
        const diffDays = getDifferenceOfDates(newCheckOut, this.moments.checkIn, 'day')
        if (diffDays < this.settings.dates.nightsMin) return
        this.moments.checkOut = newCheckOut
        this.isCLickRoleCheckIn = true
        this.reloadCalendar(this.moments.current)
        this.setCheckOutSelector()
        this.setCheckOutEvent()
        this.triggerCheckOut = false
        this.close()
    }

    setCheckInSelector() {
        if (this.settings.appeareance.mode === 'custom') return

        this.$.checkIn.value = getStringDate(this.moments.checkIn, this.settings.locale.culture)
    }

    setCheckOutSelector() {
        if (this.settings.appeareance.mode === 'custom') return
        this.$.checkOut.value = getStringDate(this.moments.checkOut, this.settings.locale.culture)
    }

    setCheckInEvent() {
        const evt = new CustomEvent('checkIn', { detail: this.setCheckInEventDetails() })
        this.$.mainElement.dispatchEvent(evt)
    }

    /**
     * @param {Date} date
     * */
    getEventDetail(date) {
        let formatedDate = getStringDate(date, this.settings.locale.culture)
        // TODO: refactor this lines
        if (this.settings.setFormat === 'YYYY-MM-DD') {
            const yyyy = getStringDate(date, this.settings.locale.culture, { year: 'numeric' })
            const mm = getStringDate(date, this.settings.locale.culture, { month: 'numeric' }) >= 10
                ? getStringDate(date, this.settings.locale.culture, { month: 'numeric' })
                : `0${getStringDate(date, this.settings.locale.culture, { month: 'numeric' })}`
            const dd = getStringDate(date, this.settings.locale.culture, { day: 'numeric' }) >= 10
                ? getStringDate(date, this.settings.locale.culture, { day: 'numeric' })
                : `0${getStringDate(date, this.settings.locale.culture, { day: 'numeric' })}`

            formatedDate = `${yyyy}-${mm}-${dd}`
        }
        const dayOfMonth = getStringDate(date, this.settings.locale.culture, { day: 'numeric' })
        const dayOfWeek = getWeekDay(date, this.settings.locale.culture, 'long')
        const month = getStringDate(date, this.settings.locale.culture, { month: 'long' })
        const year = getStringDate(date, this.settings.locale.culture, { year: 'numeric' })

        return {
            format: formatedDate,
            dayOfMonth,
            dayOfWeek,
            month,
            year,
        }
    }

    setCheckInEventDetails() {
        return {
            checkIn: true,
            ...this.getEventDetail(this.moments.checkIn),
        }
    }

    setCheckOutEventDetails() {
        return {
            checkOut: true,
            ...this.getEventDetail(this.moments.checkOut),
        }
    }

    setCheckOutEvent() {
        if (this.moments.checkOut === null) return
        const evt = new CustomEvent('checkOut', { detail: this.setCheckOutEventDetails() })
        this.$.mainElement.dispatchEvent(evt)
    }

    onClickNextMonth() {
        // If there isn't any months
        if (isSameOrAfterDate(this.moments.current, this.moments.end)) return

        this.moments.current = nextMonth(this.moments.current)
        this.deleteEvents()
        this.reloadCalendar(this.moments.current)
    }

    onClickPrevMonth() {
        if (isSameDate(this.moments.current, this.moments.start)) return

        this.moments.current = previousMonth(this.moments.current)
        this.deleteEvents()
        this.reloadCalendar(this.moments.current)
    }

    setOpenCheckInEvent() {
        const evt = new CustomEvent('openCheckIn', { detail: { selector: this.$.checkIn } })
        this.$.mainElement.dispatchEvent(evt)
    }

    setOpenCheckOutEvent() {
        const evt = new CustomEvent('openCheckOut', { detail: { selector: this.$.checkOut } })
        this.$.mainElement.dispatchEvent(evt)
    }

    // ------------------------
    // APEAREANCE FUNCTIONS
    // ------------------------
    open() {
        window.addEventListener(this.eventType, this.events.onClickOutside, true)
        this.$.dropdown.setAttribute('style', 'display:flex')
        if (this.settings.customFunctions.onOpenDatepicker) this.settings.customFunctions.onOpenDatepicker()
    }

    close() {
        window.removeEventListener(this.eventType, this.events.onClickOutside, true)
        this.$.dropdown.setAttribute('style', 'display:none')
        if (this.settings.customFunctions.onCloseDatepicker) this.settings.customFunctions.onCloseDatepicker()
        const evt = new CustomEvent('closeDatePicker')
        this.$.mainElement.dispatchEvent(evt)
        if (this.triggerCheckOut) this.setCheckOutEvent()
    }

    setDropdownBottomPosition() {
        if (isMobile()) return
        const selector = (this.isCLickRoleCheckIn) ? this.$.checkIn : this.$.checkOut
        const role = (this.isCLickRoleCheckIn) ? 'checkIn' : 'checkOut'
        let offset = selector.offsetParent.offsetLeft
        const dropDownPosition = elementPosition(this.$.dropdown)
        const { height, x } = elementPosition(selector)

        if (x + dropDownPosition.width > window.innerWidth) {
            offset -= x + dropDownPosition.width - window.innerWidth + 20
        }
        const top = selector.offsetTop + height
        this.$.dropdown.style.left = `${offset + this.settings.appeareance.offset[`${role}Left`]}px`
        this.$.dropdown.style.top = `${top + this.settings.appeareance.offset[`${role}Top`] + 3}px`
    }

    setDropdownPosition() {
        if (isMobile()) return

        if (this.settings.appeareance.orientation === 'auto' && this.orientation() === 'top') {
            this.addTopOrientation()
            return
        }

        if (this.settings.appeareance.orientation === 'auto' && this.orientation() === 'bottom') {
            this.addBottomOrientation()
            return
        }

        if (this.settings.appeareance.orientation === 'top') {
            this.addTopOrientation()
            return
        }

        if (this.settings.appeareance.orientation === 'bottom') {
            this.addBottomOrientation()
        }
    }

    addTopOrientation() {
        this.$.dropdown.classList.remove('dates-component_dropdown--bottom')
        this.$.dropdown.classList.add('dates-component_dropdown--top')

        if (isMobile()) return
        const selector = (this.isCLickRoleCheckIn) ? this.$.checkIn : this.$.checkOut
        const role = (this.isCLickRoleCheckIn) ? 'checkIn' : 'checkOut'
        let offset = selector !== null ? selector.offsetParent.offsetLeft : 0
        const dropDownPosition = elementPosition(this.$.dropdown)
        const { x } = elementPosition(selector)

        if (x + dropDownPosition.width > window.innerWidth) {
            offset -= x + dropDownPosition.width - window.innerWidth + 20
        }
        this.$.dropdown.style.left = `${offset + this.settings.appeareance.offset[`${role}Left`]}px`
        this.$.dropdown.style.top = 'unset'
    }

    addBottomOrientation() {
        this.$.dropdown.classList.remove('dates-component_dropdown--top')
        this.$.dropdown.classList.add('dates-component_dropdown--bottom')
        this.setDropdownBottomPosition()
    }

    orientation() {
        const position = Utils.position(this.$.mainElement)
        const height = window.innerHeight
        if (position.top + position.height > height / 2) {
            return 'top'
        }

        return 'bottom'
    }

    // ------------------------
    // API FUNCTIONS
    // ------------------------
    openCalendar() {
        return this
    }

    closeCalendar() {
        this.close()
    }

    // TODO: CREATE
    // setDates(checkIn, checkOut) {
    //     this.moments.checkIn = moment(checkIn, this.settings.locale.format)
    //     this.moments.checkOut = moment(checkOut, this.settings.locale.format)
    //     this.setCheckInSelector()
    //     this.setCheckOutSelector()
    //     return this
    // }

    // ----------------------
    // DATES FUNCTIONS
    // ----------------------
    /**
     *
     * @param {Date} date
     * @returns Boolean
     */
    isSelectableDay(date) {
        if (this.isCLickRoleCheckIn) {
            return this.isValidForCheckIn(date)
        }

        return this.isValidForCheckOut(date)
    }

    /**
     * @param {Date} date
     * @returns Boolean
     */
    isSelectedDay(date) {
        if (date === null || this.moments.checkIn === null || this.moments.checkOut === null) return false
        // if (this.moments.checkIn === null) return false

        // const isSameCheckInDay = isSameDate(date, this.moments.checkIn)
        // if (this.moments.checkOut === null && isSameCheckInDay) return true

        // if (isBeforeDate(date, this.moments.checkIn)) return false

        // const datePlusMinNights = sumDays(this.moments.checkIn, this.settings.dates.nightsMin)
        // if (getDifferenceOfDates(date, datePlusMinNights, 'day') <= this.settings.dates.nightsMin) return true

        // if (!this.isValidForCheckOut(date)) return false

        if (isSameDate(date, this.moments.checkIn) || isSameDate(date, this.moments.checkOut)) return false


        return isBetweenTwoDates(date, this.moments.checkIn, this.moments.checkOut)
    }

    addDaysToCheckOutMomentIfIsAfter() {
        if (this.moments.checkOut === null) {
            this.moments.checkOut = sumDate(this.moments.checkIn, 1)
            this.setCheckOutSelector()
            return
        }
        if (isSameOrAfterDate(this.moments.checkIn, this.moments.checkOut)) {
            const daysBetween = getDifferenceOfDates(this.moments.checkOut, this.moments.lastCheckIn, 'day')

            this.moments.checkOut = sumDate(this.moments.checkIn, daysBetween)
            this.setCheckOutSelector()
        }
    }

    /**
     * @param {Date} date
     * @returns Boolean
     * */
    isValidForCheckIn(date) {
        // Current is before than check in min date
        if (isBeforeDate(date, this.moments.checkInMin)) return false
        // Current + nights min is after than check in min date
        if (this.currentWithNightsMinIsAfterCheckInMax(date)) return false
        // date is enabled-for-check-in
        if (this.isEnabledForCheckIn(date)) return true
        // is valid date with minum nights on disabled dates
        if (this.isDisabledDateWithNightsMinOverDisabled(date)) return false
        // Current date + min nights is after than check-in max dates
        const nightsMinAdded = sumDays(date, this.settings.dates.nightsMin)
        if (isAfterDate(nightsMinAdded, this.moments.checkInMax)) return false
        // Current date is in a range of disabled dates
        if (this.isInDisabledDates(date)) return false


        return true
    }

    /**
     * @param {Date} date
     * @returns Boolean
     *  */
    isValidForCheckOut(date) {
        if (this.moments.checkIn === null) return false

        // Checkin date is before than date
        if (isBeforeDate(date, this.moments.checkIn)) return false
        // Current is after than check out max date
        if (isBeforeDate(date, this.moments.checkInMin)) return false

        // if (this.isInDisabledDates(date)) return false

        // Max nights from checkIn
        const isMaxNightsDiferentOfZero = this.settings.dates.nightsMax !== 0
        const maxDate = sumDays(this.moments.checkIn, this.settings.dates.nightsMax)
        if (isMaxNightsDiferentOfZero && getDifferenceOfDates(date, maxDate, 'day') > this.settings.dates.nightsMax) return false
        // Date + nights max is after than check in max
        const dateWithMaxNights = sumDays(date, this.settings.dates.nightsMax)
        if (isAfterDate(dateWithMaxNights, this.moments.checkInMax)) return false
        // date is enabled-for-check-out
        if (this.isEnabledForCheckOut(date)) return true
        // Current date is in a range of disabled dates
        if (this.dateIsAfterNextDisabledDates(date)) return false
        // if (this.isInDisabledDates(daxwte)) return false

        return true
    }

    /**
     * @param {Date} date
     * @returns Boolean
     */
    isAfterFirstDisabledDate(date) {
        const nextDisabledDate = this.moments.disabled.find(disabledDate => isAfterDate(date, disabledDate.from))

        return isSameOrAfterDate(date, nextDisabledDate.from)
    }

    /**
     * @param {Date} date
     * @returns Boolean
     */
    isEnabledForCheckIn(date) {
        if (this.settings.dates.enabledCheckIn.length === 0) return false

        return this.settings.dates.enabledCheckIn.includes(getStringDate(date))
    }

    /**
     * @param {Date} date
     * @returns Boolean
     * */
    isEnabledForCheckOut(date) {
        if (this.settings.dates.enabledCheckOut.length === 0) return false

        return this.settings.dates.enabledCheckOut.includes(getStringDate(date))
    }

    /** @param {Date} date */
    currentWithNightsMinIsAfterCheckInMax(date) {
        const dateWithMinNights = sumDays(date, this.settings.dates.nightsMin)
        return isAfterDate(dateWithMinNights, this.moments.checkInMax)
    }

    /**
     * @param {Date} date
     * @returns Boolean
     * */
    isDisabledDateWithNightsMinOverDisabled(date) {
        if (!this.moments.disabled.length) return false

        const nightsAdded = sumDays(date, this.settings.dates.nightsMin)


        // la data + minim de nits no es superior a cada inici de rang
        const isDateDisabeld = this.moments.disabled.some(range => isBetweenTwoDates(nightsAdded, range.from, range.to))

        return isDateDisabeld
    }

    /**
     * @param {Date} date
     * @returns boolean
     */
    isInDisabledDates(date) {
        if (!this.moments.disabled.length) return false

        // eslint-disable-next-line consistent-return
        const isDiabledDate = this.moments.disabled.some(range => isBetweenTwoDates(date, range.from, range.to))

        return isDiabledDate
    }

    /**
     * @param {Date} date
     * @returns boolean
     */
    dateIsAfterNextDisabledDates(date) {
        if (!this.moments.disabled.length) return false

        const nextDisabledDate = this.moments.disabled.find(disabledDate => isAfterDate(disabledDate.from, this.moments.checkOut))

        if (!nextDisabledDate) return false

        return isSameOrAfterDate(date, nextDisabledDate.from)
    }

    setCheckInFirstDate() {
        if (this.settings.dates.checkInMin !== null) {
            this.settings.dates.checkIn = this.settings.dates.checkInMin
            return this
        }

        return this
    }

    /**
     * @param {Date} checkOut
     * @returns number
     */
    getNights(checkOut) {
        return getDifferenceOfDates(checkOut, this.moments.checkIn, 'day') // + 1
    }

    // ----------------------
    // CREATE CALENDAR
    // ----------------------
    // eslint-disable-next-line
    createHeader(monthName, index) {
        const maxLength = this.settings.appeareance.monthsShowed
        const isFirst = index === 0
        const isLast = maxLength - 1 === index

        const startsHead = '<thead><tr class="dates-component_month-name"><th colspan="7">'
        const endsHead = `</th></tr><tr class="dates-component_week-days-name">${this.createWeekDaysDom()}`

        if (isMobile()) {
            return `
                ${startsHead}
                <span class="dates-component_prev-month">${arrowLeft}</span>
                ${monthName}
                <span class="dates-component_next-month">${arrowRight}</span></tr></thead>
                ${endsHead}
            `
        }

        if (isFirst) {
            return `
                ${startsHead}
                <span class="dates-component_prev-month">${arrowLeft}</span>
                ${monthName}
                ${endsHead}
            `
        }

        if (isLast) {
            return `
                ${startsHead}
                ${monthName}
                <span class="dates-component_next-month">${arrowRight}</span></tr></thead>
                ${endsHead}
            `
        }

        return `
            ${startsHead}
            ${monthName}
            ${endsHead}
        `
    }

    // eslint-disable-next-line
    createWeekDaysDom() {
        const weekDays = getWeekDays('short', this.settings.locale.culture)

        return weekDays.map(day => `<th>${day}</th>`).join('')
    }

    /**
     * @param {(Date|null)} jumpTo
     * @returns String
    */
    createMonthsDom(jumpTo = null) {
        const startMonth = jumpTo || this.moments.start
        const calendars = this.settings.appeareance.monthsShowed

        const calendarMonths = Array.from({ length: calendars }, (_, index) => sumDate(startMonth, index, 'month'))

        const monthsStrings = calendarMonths.map((month, index) => {
            const monthDays = this.createMonth(month)
            const monthName = getMonthAndYear(month, this.settings.locale.culture)

            return `
                <div class="dates-component_table">
                    <table id="${monthName.replaceAll(' ', '-')}" class="dates-component_month">
                        ${this.createHeader(monthName, index)} 
                        ${this.createMonthDom(monthDays)}
                    </table>
                </div>
            `
        })

        return monthsStrings.join('')
    }

    // CREATE MONTH ARRAY
    /** @param {Date} month */
    createMonth(month) {
        const calendarMonthDates = getDatesStartingFirstMondayOfMonth(month)

        // Group dates by month
        const daysOfPrevMonth = calendarMonthDates.filter(date => (date.getMonth() === (month.getMonth() - 1))
            || (month.getMonth() === 0 && date.getMonth() === 11))
        const daysOfThisMonth = calendarMonthDates.filter(date => date.getMonth() === month.getMonth())
        const daysOfNextMonth = calendarMonthDates.filter(date => date.getMonth() === (month.getMonth() + 1)
            || (date.getMonth() === 0 && month.getMonth() === 11))

        const daysFromPreviousMonth = this.createRemainderDays(daysOfPrevMonth)
        const daysFromThisMonth = this.createCurrentMonthDays(daysOfThisMonth)

        const daysFromNextMonth = this.createNextMonthDays(daysOfNextMonth)

        const allDatesOfMonth = [ ...daysFromPreviousMonth, ...daysFromThisMonth, ...daysFromNextMonth ]

        return allDatesOfMonth
    }

    // PREPEND REMINDER DAYS
    /**
     * @param {Date[]} datesPrevMonth
     * @returns MonthDay[]
     * */
    createRemainderDays(datesPrevMonth) {
        const monthDays = datesPrevMonth.map((date) => {
            const isDayActive = this.isSelectableDay(new Date(date))
            const isDaySelectable = this.isSelectedDay(new Date(date))

            const activeClass = isDayActive
                ? 'dates-component_day dates-component_old'
                : 'dates-component_day dates-component_unselectable dates-component_old'
            const selectClass = isDaySelectable
                ? 'dates-component_selected'
                : ''

            return {
                date,
                class: `${activeClass} ${selectClass}`,
            }
        })

        return monthDays
    }

    // CREATE CURRENT MONTH DAYS
    /**
     * @param {Date[]} datesCurrentMonth
     * @returns MonthDay[]
     * */
    createCurrentMonthDays(datesCurrentMonth) {
        const monthDays = datesCurrentMonth.map(date => ({
            date,
            class: this.currentMonthClasses(new Date(date)),
        }))

        return monthDays
    }

    /**
     *  @param {Date} date
     *  @returns string
    */
    currentMonthClasses(date) {
        const isDayActive = this.isSelectableDay(new Date(date))
        const isSelectedClass = this.isSelectedDay(new Date(date))
        const isCheckInDay = isSameDate(date, this.moments.checkIn)
        const isCheckOutDay = isSameDate(date, this.moments.checkOut)


        const selectableClass = isDayActive
            ? 'dates-component_day'
            : 'dates-component_day dates-component_unselectable'
        const checkInClass = isCheckInDay && isDayActive ? 'dates-component_check-in-day' : ''
        const checkOutClass = isCheckOutDay && isDayActive ? 'dates-component_check-out-day' : ''
        const selectedClass = isSelectedClass ? 'dates-component_selected' : ''


        return `${checkInClass} ${checkOutClass} ${selectableClass} ${selectedClass}`
    }

    /**
     * @param {Date[]} dates
     * @returns MonthDay[]
     */
    createNextMonthDays(dates) {
        const response = dates.map((date) => {
            const isDayActive = this.isSelectableDay(new Date(date))
            const isSelectedClass = this.isSelectedDay(new Date(date))

            const selectableClass = isDayActive
                ? 'dates-component_new dates-component_day'
                : 'dates-component_unselectable dates-component_new dates-component_day'
            const selectedClass = isSelectedClass ? 'dates-component_selected' : ''

            return {
                date,
                class: `${selectableClass} ${selectedClass}`,
            }
        })

        return response
    }


    /**
     * @param {MonthDay[]} dates
     * @returns string
     */
    createMonthDom(dates) {
        const endWeek = number => number % 7 === 0
        const startWeek = number => number % 7 === 1
        const createDay = (date, calss) => {
            const dayNumber = date.getDate()
            const stringDate = new Intl.DateTimeFormat('en', {
                year: 'numeric',
                month: '2-digit',
                day: '2-digit',
            }).format(date)

            return `<td data-date="${stringDate}" class="${calss}">${dayNumber}</td>`
        }

        const monthStrings = dates.map((day, index) => {
            const weekNumber = index + 1

            if (startWeek(weekNumber)) {
                return `<tr> ${createDay(day.date, day.class)}`
            }

            if (endWeek(weekNumber)) {
                return `${createDay(day.date, day.class)} </tr>`
            }

            return createDay(day.date, day.class)
        })

        return monthStrings.join('')
    }

    /**
     * @param {Date} checkIn
     * @param {Date} checkOut
     * @returns {boolean}
     */
    isValid(checkIn, checkOut) {
        return this.isValidForCheckIn(checkIn) && this.isValidForCheckOut(checkOut) && isBeforeDate(checkIn, checkOut)
    }

    /**
     * @param {Date} checkIn
     * @param {Date} checkOut
     */
    setDates(checkIn, checkOut) {
        if (!this.isValid(checkIn, checkOut)) {
            console.error('Check in or check out invalid')
            return
        }

        this.moments.checkIn = checkIn
        this.moments.checkOut = checkOut
        this.moments.current = checkIn

        this.setCheckInSelector()
        this.setCheckOutSelector()

        this.setCheckInEvent()
        this.setCheckOutEvent()
    }
}

export default DatesComponent
