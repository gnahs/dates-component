
/**
 * @typedef {Object} SettingsLocale
 * @property {string} culture
 * @property {string} language
 * @property {string} format
 * @property {any} literals
 */

/**
 * @typedef {Object} DisabledDates
 * @property {string} from
 * @property {string} to
 */

/**
 * @typedef {Object} SettingsDates
 * @property {Date} start
 * @property {Date} end
 * @property {(Date|null)} checkIn
 * @property {(Date|null)} checkOut
 * @property {Date} checkInMin
 * @property {Date} checkInMax
 * @property {number} nightsMin
 * @property {number} nightsMax
 * @property {string[]} enabledCheckIn
 * @property {string[]} enabledCheckOut
 * @property {DisabledDates[]} disabled
 */

/**
 * @typedef {Object} SettingsAppeareanceOffset
 * @property {number} checkInTop
 * @property {number} checkInLeft
 * @property {number} checkOutTop
 * @property {number} checkOutLeft
 */

/**
 * @typedef {Object} SettingsAppeareance
 * @property {string} mode
 * @property {number} monthsShowed
 * @property {boolean} showNights
 * @property {boolean} showMoon
 * @property {boolean} expanded
 * @property {boolean} autoClose
 * @property {boolean|null} checkInSelector
 * @property {boolean|null} checkOutSelector
 * @property {SettingsAppeareanceOffset} offset
 */

/**
 * @typedef {Object} SettingsCustomFunctions
 * @property {Function} onDayClick
 * @property {Function} onOpenDatepicker
 * @property {Function} onCloseDatepicker
 */

/**
 * @typedef {Object} Settings
 * @property {SettingsDates} dates
 * @property {SettingsLocale} locale
 * @property {SettingsAppeareance} appeareance
 * @property {string[]} allowedCultures
 * @property {SettingsCustomFunctions} customFunctions
 */
