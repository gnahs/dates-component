/**
 * @typedef {Object} DisabledMoments
 * @property {Date} from
 * @property {Date} to
 */

/**
 * @typedef {Object} Moments
 * @property {Date} start
 * @property {Date} end
 * @property {Date} checkIn
 * @property {Date} checkOut
 * @property {Date} start
 * @property {Date} checkInMin
 * @property {Date} checkInMax
 * @property {Date} lastCheckIn
 * @property {Date} current
 * @property {DisabledMoments[]} disabled
 */
