import Literals from './locales/locales'

export default {
    // LOCALES
    allowedCultures: [ 'en', 'en-au', 'en-ca', 'en-gb', 'es', 'es-co', 'ca', 'fr', 'fr-ca', 'de', 'de-at', 'ru', 'nl', 'it', 'pt', 'pt-br', 'es-ec', 'es-ar', 'pt-br' ],
    locale: {
        format: 'YYYY-MM-DD',
        language: 'es',
        culture: 'auto',
        literals: Literals,
    },
    // DATE SETTINGS
    dates: {
        checkIn: null,
        checkOut: null,
        checkInMin: null,
        checkInMax: null,
        nightsMin: 1,
        nightsMax: 0,
        disabled: [], // {from:2019-09-02, to: 2019-09-12}
        enabledCheckIn: [], // according to the locale format
        enabledCheckOut: [], // according to the locale format
        disabledWeekDays: [],
        busy: [], // {from:2019-09-02, to: 2019-09-12} FEATURE
        single: false,
        end: null,
        setFormat: 'YYYY-MM-DD',
    },
    // APPEREANCE
    appeareance: {
        // input / custom / default
        mode: 'input',
        orientation: 'bottom', // Auto, top, bottom
        monthsShowed: 2,
        showNights: true,
        showMoon: true,
        expanded: false,
        autoClose: true,
        checkInSelector: null,
        checkOutSelector: null,
        offset: {
            checkInTop: 0,
            checkInLeft: 0,
            checkOutTop: 0,
            checkOutLeft: 0,
        },
    },
    // CUSTOM FUNCTIONS
    customFunctions: {
        onDayClick: false,
        onOpenDatepicker: false,
        onCloseDatepicker: false,
    },

}
