const arrowRight = `
<?xml version="1.0" encoding="utf-8"?>
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
    viewBox="0 0 10 10" style="enable-background:new 0 0 10 10;" xml:space="preserve">
<g>
    <path id="Trazado_512" d="M3.1,0.7L2.7,1.1C2.5,1.3,2.5,1.5,2.5,1.7s0.1,0.4,0.2,0.5L5.2,5L2.7,7.8C2.5,8,2.5,8.1,2.5,8.3
        s0.1,0.4,0.2,0.5l0.4,0.5c0.1,0.1,0.2,0.2,0.4,0.2S3.8,9.4,4,9.3l3.3-3.8C7.5,5.4,7.5,5.2,7.5,5S7.4,4.6,7.3,4.5L4,0.7
        C3.9,0.6,3.7,0.5,3.5,0.5S3.2,0.6,3.1,0.7L3.1,0.7z"/>
</g>
</svg>
`

const arrowDown = `<?xml version="1.0" encoding="utf-8"?>
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 10 10" style="enable-background:new 0 0 10 10;" xml:space="preserve">
<g><path d="M9.3,3.1L8.9,2.7C8.7,2.5,8.5,2.5,8.3,2.5c-0.2,0-0.4,0.1-0.5,0.2L5,5.2L2.2,2.7
        C2,2.5,1.9,2.5,1.7,2.5c-0.2,0-0.4,0.1-0.5,0.2L0.7,3.1C0.6,3.2,0.5,3.3,0.5,3.5c0,0.2,0.1,0.3,0.2,0.5l3.8,3.3
        C4.6,7.5,4.8,7.5,5,7.5c0.2,0,0.4-0.1,0.5-0.2L9.3,4c0.1-0.1,0.2-0.3,0.2-0.5C9.5,3.3,9.4,3.2,9.3,3.1L9.3,3.1z"/></g>
</svg>`

const arrowLeft = `
<?xml version="1.0" encoding="utf-8"?>
<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
    viewBox="0 0 10 10" style="enable-background:new 0 0 10 10;" xml:space="preserve">
<g>
    <path d="M6.9,9.3l0.4-0.4c0.2-0.2,0.2-0.4,0.2-0.6S7.4,7.9,7.3,7.8L4.8,5l2.5-2.8C7.5,2,7.5,1.9,7.5,1.7
        S7.4,1.3,7.3,1.2L6.9,0.7C6.8,0.6,6.7,0.5,6.5,0.5C6.3,0.5,6.2,0.6,6,0.7L2.7,4.5C2.5,4.6,2.5,4.8,2.5,5s0.1,0.4,0.2,0.5L6,9.3
        c0.1,0.1,0.3,0.2,0.5,0.2C6.7,9.5,6.8,9.4,6.9,9.3L6.9,9.3z"/>
</g>
</svg>
`

const close = `<div class="dates-component_close">
<?xml version="1.0" encoding="utf-8"?>
<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
viewBox="0 0 30 30" style="enable-background:new 0 0 30 30;" xml:space="preserve">
<g transform="translate(-284 -408.89)">
<path d="M307,417.5l-1.6-1.6l-6.4,6.4l-6.4-6.4l-1.6,1.6l6.4,6.4l-6.4,6.4l1.6,1.6l6.4-6.4l6.4,6.4l1.6-1.6l-6.4-6.4L307,417.5z"/>
</g>
</svg>
</div>`

const moon = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 330 310">
<defs><style>.cls-1{isolation:isolate;}.cls-2{mix-blend-mode:multiply;}</style></defs>
<title>moon</title><g class="cls-1"><g id="Capa_1" data-name="Capa 1">
<path class="cls-2" d="M94,78a137.51,137.51,0,0,1,11.79-55.88A138,138,0,1,0,282.71,206.38,138.08,138.08,0,0,1,94,78Z"/><path d="M183,121.5H137.5A5,5,0,0,1,134,113l37-37H137.5a5,5,0,0,1,0-10H183a5,5,0,0,1,3.54,8.54l-37,37H183a5,5,0,0,1,0,10Z"/><path d="M301,110.5H255.5A5,5,0,0,1,252,102l37-37H255.5a5,5,0,0,1,0-10H301a5,5,0,0,1,3.54,8.54l-37,37H301a5,5,0,0,1,0,10Z"/>
<path d="M237,144.5H202.5A5,5,0,0,1,199,136l26-26H202.5a5,5,0,0,1,0-10H237a5,5,0,0,1,3.54,8.54l-26,26H237a5,5,0,0,1,0,10Z"/></g></g></svg>`

export {
    arrowDown,
    arrowLeft,
    arrowRight,
    close,
    moon,
}
