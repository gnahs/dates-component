import {
    CHECK_IN_DAY_CLASS,
    CHECK_IN_INPUT_SELECTOR,
    CHECK_OUT_DAY_CLASS,
    CHECK_OUT_INPUT_SELECTOR,
    SELECTED_DAY_SELECTOR,
    UNSELECTABLE_DAY_CLASS,
    UNSELECTABLE_DAY_SELECTOR,
} from '../constants'

/* eslint-disable no-undef */
describe('Date component', () => {
    beforeEach(() => {
        cy.visit('test.html')

        cy.window().then((window) => {
            // eslint-disable-next-line no-new
            new window.DatesComponent('.dates-component', {
                locale: {
                    language: 'ca',
                    culture: 'ca',
                    format: 'YYYY-MM-DD',
                },
                dates: {
                    nightsMin: 1,
                    checkIn: '2022-02-22',
                    checkOut: '2022-02-23',
                    start: '2022-02-22',
                },
                appeareance: {
                    mode: 'input',
                    monthsShowed: 2,
                    checkInSelector: '.checin',
                    checkOutSelector: '.checout',
                },
            })
        })
    })

    it('init component', () => {
        cy
            .get(CHECK_IN_INPUT_SELECTOR)
            .should('have.value', '22/02/2022')

        cy
            .get(CHECK_OUT_INPUT_SELECTOR)
            .should('have.value', '23/02/2022')
    })

    it('open dropdown when click checkin', () => {
        cy
            .get(CHECK_IN_INPUT_SELECTOR)
            .click()

        cy
            .get('.dates-component_dropdown')
            .should('be.visible')
    })

    it('open dropdown when click checkout', () => {
        cy
            .get(CHECK_OUT_INPUT_SELECTOR)
            .click()

        cy
            .get('.dates-component_dropdown')
            .should('be.visible')
    })

    it('select checkin day', () => {
        cy
            .get(CHECK_IN_INPUT_SELECTOR)
            .click()

        cy
            .get('[data-date="2022-02-22"]')
            .click()

        cy
            .get('[data-date="2022-02-22"]')
            .should('have.class', CHECK_IN_DAY_CLASS)
        cy
            .get('[data-date="2022-02-23"]')
            .should('have.class', CHECK_OUT_DAY_CLASS)

        cy
            .get('.dates-component_unselectable')
            .should('have.length', 22)

        cy
            .get(CHECK_IN_INPUT_SELECTOR)
            .should('have.value', '22/02/2022')

        cy
            .get(CHECK_OUT_INPUT_SELECTOR)
            .should('have.value', '23/02/2022')
    })

    it('select checkout day', () => {
        cy
            .get(CHECK_OUT_INPUT_SELECTOR)
            .click()

        cy
            .get('[data-date="2022-02-25"]')
            .click()

        cy
            .get('[data-date="2022-02-25"]')
            .should('have.class', CHECK_OUT_DAY_CLASS)

        cy
            .get('[data-date="2022-02-22"]')
            .should('have.class', CHECK_IN_DAY_CLASS)

        cy
            .get(UNSELECTABLE_DAY_SELECTOR)
            .should('have.length', 22)

        cy
            .get('.dates-component_dropdown')
            .should('not.be.visible')

        cy
            .get(CHECK_IN_INPUT_SELECTOR)
            .should('have.value', '22/02/2022')

        cy
            .get(CHECK_OUT_INPUT_SELECTOR)
            .should('have.value', '25/02/2022')

        cy
            .get(SELECTED_DAY_SELECTOR)
            .should('have.length', 4)
    })
})

describe('Dropdown', () => {
    beforeEach(() => {
        cy.visit('test.html')
        cy.window().then((window) => {
            // eslint-disable-next-line no-new
            new window.DatesComponent('.dates-component', {
                locale: {
                    language: 'ca',
                    culture: 'ca',
                    format: 'YYYY-MM-DD',
                },
                dates: {
                    nightsMin: 1,
                    checkIn: '2022-02-22',
                    checkOut: '2022-02-23',
                    start: '2022-02-22',
                },
                appeareance: {
                    mode: 'input',
                    monthsShowed: 2,
                    checkInSelector: '.checin',
                    checkOutSelector: '.checout',
                },
            })

            cy
                .get(CHECK_IN_INPUT_SELECTOR)
                .click()
        })
    })

    it('shows month names', () => {
        cy.contains('febrer 2022')
        cy.contains('març 2022')
    })

    it('shows weekdays', () => {
        const WEEK_DAYS = [
            'dl', 'dt', 'dc', 'dj', 'dv', 'ds', 'dg',
            'dl', 'dt', 'dc', 'dj', 'dv', 'ds', 'dg',
        ]

        const weekDays = cy
            .get('.dates-component_week-days-name > th')
            .should('have.length', 14)

        weekDays
            .each((value, index) => {
                expect(value.text()).to.be.equal(WEEK_DAYS[index])
            })
    })

    it('move to next month', () => {
        cy
            .get('.dates-component_next-month')
            .click()


        cy.contains('març 2022')
        cy.contains('abril 2022')
    })

    it('shows correct number of calendars', () => {
        cy
            .get('.dates-component_table')
            .should('have.length', 2)
    })

    it('previous days on start day is it unselectable', () => {
        cy
            .get(UNSELECTABLE_DAY_SELECTOR)
            .should('have.length', 22)

        const UNSELECTABLE_DATES = [
            '2022-01-31', '2022-02-01', '2022-02-02', '2022-02-03', '2022-02-04', '2022-02-05', '2022-02-06',
            '2022-02-07', '2022-02-08', '2022-02-09', '2022-02-10', '2022-02-11', '2022-02-12', '2022-02-13',
            '2022-02-14', '2022-02-15', '2022-02-16', '2022-02-17', '2022-02-18', '2022-02-19', '2022-02-20',
            '2022-02-21',
        ]

        UNSELECTABLE_DATES.forEach((date) => {
            cy
                .get(`[data-date="${date}"]`)
                .should('have.class', UNSELECTABLE_DAY_CLASS)
        })
    })
})
