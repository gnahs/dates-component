/* eslint-disable no-new */
/* eslint-disable no-undef */
import {
    CHECK_IN_DAY_CLASS,
    CHECK_IN_INPUT_SELECTOR,
    CHECK_OUT_DAY_CLASS,
    CHECK_OUT_INPUT_SELECTOR,
    SELECTED_DAY_CLASS,
    UNSELECTABLE_DAY_CLASS,
    UNSELECTABLE_DAY_SELECTOR,
} from '../../constants'

describe('disable range of days', () => {
    beforeEach(() => {
        cy.visit('test.html')
    })

    describe('disable one range', () => {
        const disabledOneRangeSettings = {
            locale: {
                language: 'en',
                culture: 'en',
                format: 'YYYY-MM-DD',
            },
            dates: {
                nightsMin: 1,
                checkIn: '2022-02-22',
                checkOut: '2022-02-23',
                start: '2022-02-22',
                disabled: [
                    {
                        from: '2022-03-01',
                        to: '2022-03-07',
                    },
                ],
            },
            appeareance: {
                mode: 'input',
                monthsShowed: 2,
                checkInSelector: '.checin',
                checkOutSelector: '.checout',
            },
        }
        beforeEach(() => {
            cy.window().then((window) => {
                new window.DatesComponent('.dates-component', disabledOneRangeSettings)
            })
        })

        it('disable days of range', () => {
            const DISABLED_DATES = [ '2022-03-01', '2022-03-02', '2022-03-03', '2022-03-04', '2022-03-05', '2022-03-06', '2022-03-07' ]

            DISABLED_DATES.forEach((date) => {
                cy
                    .get(`[data-date="${date}"]`)
                    .should('have.class', UNSELECTABLE_DAY_CLASS)
            })
        })

        it('and select checkIn between disabled range', () => {
            cy
                .get(CHECK_IN_INPUT_SELECTOR)
                .click()

            cy
                .get('[data-date="2022-03-05"]')
                .eq(1)
                .click()

            // Expect checkIn to be 2022-02-22 (same as start)
            cy
                .get('[data-date="2022-02-22"]')
                .contains('22')
                .should('have.class', SELECTED_DAY_CLASS)
                .should('have.class', CHECK_IN_DAY_CLASS)
        })

        it('and select checkIn on start disabled range', () => {
            cy
                .get(CHECK_IN_INPUT_SELECTOR)
                .click()

            cy
                .get('[data-date="2022-03-01"]')
                .eq(1)
                .click()

            // Expect checkIn to be 2022-02-22 (same as start)
            cy
                .get('[data-date="2022-02-22"]')
                .contains('22')
                .should('have.class', SELECTED_DAY_CLASS)
                .should('have.class', CHECK_IN_DAY_CLASS)
        })

        it('and select checkIn on end disabled range', () => {
            cy
                .get(CHECK_IN_INPUT_SELECTOR)
                .click()

            cy
                .get('[data-date="2022-03-07"]')
                .eq(1)
                .click()

            // Expect checkIn to be 2022-02-22 (same as start)
            cy
                .get('[data-date="2022-02-22"]')
                .contains('22')
                .should('have.class', SELECTED_DAY_CLASS)
                .should('have.class', CHECK_IN_DAY_CLASS)
        })

        it('and select checkout between disabled range', () => {
            cy
                .get(CHECK_OUT_INPUT_SELECTOR)
                .click()

            cy
                .get('[data-date="2022-03-05"]')
                .eq(1)
                .click()

            // Expect checkout to be 2022-02-23 (same as start)
            cy
                .get('[data-date="2022-02-23"]')
                .contains('23')
                .should('have.class', SELECTED_DAY_CLASS)
                .should('have.class', CHECK_OUT_DAY_CLASS)
        })

        it('and select checkout on start disabled range', () => {
            cy
                .get(CHECK_OUT_INPUT_SELECTOR)
                .click()

            cy
                .get('[data-date="2022-03-01"]')
                .eq(1)
                .click()

            // Expect checkIn to be 2022-02-22 (same as start)
            cy
                .get('[data-date="2022-02-23"]')
                .contains('23')
                .should('have.class', SELECTED_DAY_CLASS)
                .should('have.class', CHECK_OUT_DAY_CLASS)
        })

        it('and select checkout on end disabled range', () => {
            cy
                .get(CHECK_OUT_INPUT_SELECTOR)
                .click()

            cy
                .get('[data-date="2022-03-07"]')
                .eq(1)
                .click()

            // Expect checkout to be 2022-02-23 (same as start)
            cy
                .get('[data-date="2022-02-23"]')
                .contains('23')
                .should('have.class', SELECTED_DAY_CLASS)
                .should('have.class', CHECK_OUT_DAY_CLASS)
        })

        it('and selected day is between disabled range', () => {
            cy
                .get(CHECK_OUT_INPUT_SELECTOR)
                .click()

            cy
                .get('[data-date="2022-03-10"]')
                .eq(1)
                .click()

            // Expect checkout to be 2022-02-23 (same as start)
            cy
                .get('[data-date="2022-02-23"]')
                .contains('23')
                .should('have.class', SELECTED_DAY_CLASS)
                .should('have.class', CHECK_OUT_DAY_CLASS)
        })

        it('and select checkout disable all days after disabled range', () => {
            cy
                .get(CHECK_OUT_INPUT_SELECTOR)
                .click()

            // Select disabled days
            cy
                .get(UNSELECTABLE_DAY_SELECTOR)
                .should('have.length', 75)
        })
    })

    describe('disable two range or more', () => {
        const disabledTwoRangeSettings = {
            locale: {
                language: 'en',
                culture: 'en',
                format: 'YYYY-MM-DD',
            },
            dates: {
                nightsMin: 1,
                checkIn: '2022-02-22',
                checkOut: '2022-02-23',
                start: '2022-02-22',
                disabled: [
                    {
                        from: '2022-03-01',
                        to: '2022-03-07',
                    },
                    {
                        from: '2022-03-15',
                        to: '2022-03-18',
                    },
                ],
            },
            appeareance: {
                mode: 'input',
                monthsShowed: 2,
                checkInSelector: '.checin',
                checkOutSelector: '.checout',
            },
        }
        beforeEach(() => {
            cy.window().then((window) => {
                new window.DatesComponent('.dates-component', disabledTwoRangeSettings)
            })
        })

        it('disable all ranges days', () => {
            const DISABLED_DATES = [ '2022-03-01', '2022-03-02', '2022-03-03', '2022-03-04', '2022-03-05', '2022-03-06', '2022-03-07', '2022-03-15', '2022-03-16', '2022-03-17', '2022-03-18' ]

            DISABLED_DATES.forEach((date) => {
                cy
                    .get(`[data-date="${date}"]`)
                    .should('have.class', UNSELECTABLE_DAY_CLASS)
            })
        })
    })
})
