/* eslint-disable no-undef */
import moment from 'moment'

describe('Set different language and culture', () => {
    beforeEach(() => {
        cy.visit('test.html')
        cy.window().then((window) => {
            // eslint-disable-next-line no-new
            new window.DatesComponent('.dates-component', {
                locale: {
                    language: 'en',
                    culture: 'en',
                    format: 'YYYY-MM-DD',
                },
                dates: {
                    nightsMin: 1,
                    checkIn: moment().format('YYYY-MM-DD'),
                    checkOut: moment().add(1, 'days').format('YYYY-MM-DD'),
                    start: moment().format('YYYY-MM-DD'),
                },
                appeareance: {
                    mode: 'input',
                    monthsShowed: 2,
                    checkInSelector: '.checin',
                    checkOutSelector: '.checout',
                },
            })

            cy
                .get('.dates-component_check-in')
                .click()
        })
    })

    it('shows month names', () => {
        cy.contains('february 2022')
        cy.contains('march 2022')
    })

    it('shows weekdays', () => {
        const WEEK_DAYS = [
            'Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa',
            'Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa',
        ]

        const weekDays = cy.get('.dates-component_week-days-name > th')

        weekDays
            .each((value, index) => {
                expect(value.text()).to.be.equal(WEEK_DAYS[index])
            })
    })
})

describe('Set different format', () => {
    beforeEach(() => {
        cy.visit('test.html')
        cy.window().then((window) => {
            // eslint-disable-next-line no-new
            new window.DatesComponent('.dates-component', {
                locale: {
                    language: 'ca',
                    culture: 'ca',
                    format: 'YYYY-MM-DD',
                },
                dates: {
                    nightsMin: 1,
                    checkIn: moment().format('YYYY-MM-DD'),
                    checkOut: moment().add(1, 'days').format('YYYY-MM-DD'),
                    start: moment().format('YYYY-MM-DD'),
                },
                appeareance: {
                    mode: 'input',
                    monthsShowed: 2,
                    checkInSelector: '.checin',
                    checkOutSelector: '.checout',
                },
            })

            cy
                .get('.dates-component_check-in')
                .click()
        })
    })

    it('Date of custom event have correct format', () => {
        cy
            .get('.dates-component.dates-component--input')
            .then(($el) => {
                $el
                    .get(0)
                    .addEventListener('checkIn', (ev) => {
                        const dateFormated = ev.detail.format
                        expect(dateFormated).to.be.equal('2022-02-22')
                    })
            })

        cy
            .get('[data-date="2022-02-22"]')
            .click()
    })
})
