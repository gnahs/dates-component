/* eslint-disable import/prefer-default-export */

// INPUTS
export const CHECK_IN_INPUT_SELECTOR = '.dates-component_check-in'
export const CHECK_OUT_INPUT_SELECTOR = '.dates-component_check-out'

// DAYS
export const CHECK_IN_DAY_CLASS = 'dates-component_check-in-day'
export const CHECK_OUT_DAY_CLASS = 'dates-component_check-out-day'
export const SELECTED_DAY_CLASS = 'dates-component_selected'
export const SELECTED_DAY_SELECTOR = `.${SELECTED_DAY_CLASS}`
export const UNSELECTABLE_DAY_CLASS = 'dates-component_unselectable'
export const UNSELECTABLE_DAY_SELECTOR = `.${UNSELECTABLE_DAY_CLASS}`
